api/1.0/{user_type}/{action}

api/1.0/{user_type}/register [POST]
api/1.0/{user_type}/auth [POST]

api/1.0/common [GET]
api/1.0/common/pub [POST]
api/1.0/common/pub/{pub_id} [GET]
api/1.0/common/pub/{pub_id}/comment [POST]
api/1.0/common/complaints [GET] [POST]
api/1.0/common/requests [GET] [POST]

api/1.0/admin [GET]
api/1.0/admin/requests [GET]
api/1.0/admin/requests/accept [POST]

api/1.0/adv [GET]
api/1.0/adv/requests [GET] [POST]

