package edu.spbstu.arch.model;

import edu.spbstu.arch.db.entity.*;
import edu.spbstu.arch.db.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class ModelLogic {
    private UserAdvertiserRepository advertisers;
    private UserCommonRepository commonUsers;
    private UserAdminRepository admins;
    private PublicationRepository publications;
    private CommentRepository comments;

    @Autowired
    public ModelLogic(UserCommonRepository commonUsersRepository,
                      UserAdvertiserRepository advertisersRepository,
                      UserAdminRepository adminsRepository,
                      PublicationRepository publications,
                      CommentRepository comments) {
        this.commonUsers = commonUsersRepository;
        this.advertisers = advertisersRepository;
        this.admins = adminsRepository;
        this.publications = publications;
        this.comments = comments;
    }

    public User loginAdvertiser(String login, String pwd) {
        User user = advertisers.getByLogin(login).orElse(null);
        return user != null && user.loginUser(pwd) ? user : null;
    }

    public User loginCommon(String login, String pwd) {
        User user = commonUsers.getByLogin(login).orElse(null);
        return user != null && user.loginUser(pwd) ? user : null;
    }

    public User loginAdmin(String login, String pwd) {
        User user = admins.getByLogin(login).orElse(null);
        return user != null && user.loginUser(pwd) ? user : null;
    }

    public void addNewUser(UserCommon commonUser){
        commonUsers.saveAndFlush(commonUser);
    }

    public void addNewAdvertiser(UserAdvertiser advertiser){
        advertisers.saveAndFlush(advertiser);
    }

    public void addNewAdministrator(UserAdmin admin){
        admins.saveAndFlush(admin);
    }

    public Publication newPublication(Publication pub) {
        return publications.saveAndFlush(pub);
    }

    public Publication getPublicationForUser(Integer pubId, String userLogin) {
        UserCommon user = commonUsers.getByLogin(userLogin).orElse(null);
        if (user == null) {
            return null;
        } else {
            Publication pub = publications.findById(pubId).orElse(null);
            UserCommon author = pub == null ? null : pub.getAuthor();
            if (pub != null && (!pub.isOnlySubs() || author == user || user.getSubs().contains(author)))
                return pub;
            else
                return null;
        }
    }

    public boolean leaveACommentOnPublication(Integer pubId, Comment comment) {
        Publication publication = publications.findById(pubId).orElse(null);
        if (publication == null)
            return false;

        comments.save(comment);
        publication.getComments().add(comment);
        publications.save(publication);
        return true;
    }

    public List<Publication> getListPublicationsForUser(String userLogin) {
        UserCommon user = commonUsers.getByLogin(userLogin).orElse(null);
        if (user == null) {
            return null;
        } else {
            PageRequest pages = PageRequest.of(0, 10);
            Set<Integer> subs = user.getSubs().stream().map(UserCommon::getId).collect(Collectors.toSet());
            //subs.add(user.getId()); // hack
            if (subs.isEmpty())
                return publications.getLastPublicationsNoSubs(user, pages).getContent();
            else
                return publications.getLastPublications(user, user.getSubs(), pages).getContent();
        }
    }

    public List<Publication> getPublicationsFrom(String userLogin, String authorLogin) {
        UserCommon user = commonUsers.getByLogin(userLogin).orElse(null);
        UserCommon author = commonUsers.getByLogin(authorLogin).orElse(null);
        if (author == null || user == null) {
            return null;
        } else {
            Set<UserCommon> subs = user.getSubs();
            if (subs.isEmpty())
                return publications.getAllByAuthorNoSubs(author, user);
            else
                return publications.getAllByAuthor(author, user, subs);
        }
    }

    public UserCommon getUserInfo(Integer id) {
        return commonUsers.findById(id).orElse(null);
    }

    public boolean subscribeFor(Integer toUser, Integer fromUser, boolean unSubscribe) {
        UserCommon to = commonUsers.findById(toUser).orElse(null);
        UserCommon from = commonUsers.findById(fromUser).orElse(null);
        if (to == null || from == null || to.getId() == from.getId())
            return false;

        // no complex actions
        if (unSubscribe)
            from.getSubs().remove(to);
        else
            from.getSubs().add(to);

        commonUsers.saveAndFlush(from);
        return true;
    }
}
