package edu.spbstu.arch.model;

import java.util.Random;

/**
 * Имитация сервиса оплаты
 */
public class PaymentService {
    private PaymentService() {
        connect();
    }

    private static PaymentService instance = null;
    
    public static synchronized PaymentService getInstance() {
        if(instance == null)
            instance = new PaymentService();

        return instance;
    }
    
    private void connect() {}

    public boolean payFor(String payInfo, float cost, String text) {
        boolean success = new Random().nextInt(100) > 70;
        System.out.println("PaymentService::tryToPay = " + (success ? "T" : "F"));
        return success;
    }
}
