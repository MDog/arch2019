package edu.spbstu.arch.db.entity;

import lombok.Data;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("1")
@Data
public class UserAdvertiser extends User {
    private String name;
    private String contacts;
}
