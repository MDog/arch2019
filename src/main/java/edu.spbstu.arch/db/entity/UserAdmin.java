package edu.spbstu.arch.db.entity;

import lombok.Data;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("0")
@Data
public class UserAdmin extends User {
}
