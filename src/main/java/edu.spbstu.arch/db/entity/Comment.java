package edu.spbstu.arch.db.entity;

import lombok.Data;
import lombok.Getter;

import javax.persistence.*;
import java.util.Date;

@Entity
@Data
@Table(name = "comments")
public class Comment {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    // дата и время появления комментария
    @Column(updatable = false, nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    @Getter
    private Date creationTime = new Date();

    // автор
    @ManyToOne
    @Getter
    private UserCommon author;

    // текст комментария
    @Column(nullable = false)
    @Getter
    private String text;
}
