package edu.spbstu.arch.db.entity;

import lombok.Data;
import lombok.Getter;
import org.hibernate.annotations.Cascade;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@Data
@Table(name = "publications")
public class Publication {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    // дата публикации
    @Column(updatable = false, nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    @Getter
    private Date creationTime = new Date();

    // автор
    @ManyToOne
    @Cascade(org.hibernate.annotations.CascadeType.SAVE_UPDATE)
    @Getter
    private UserCommon author;

    // заголовок
    @Getter
    private String title;

    // контент
    @Getter
    private String url;

    // только для подписчиков автора
    @Getter
    private boolean onlySubs;

    @OneToMany
    private List<Comment> comments;
}
