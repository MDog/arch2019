package edu.spbstu.arch.db.entity;

import lombok.Data;
import org.hibernate.annotations.Cascade;

import javax.persistence.*;
import java.util.Set;

@Entity
@DiscriminatorValue("2")
@Data
public class UserCommon extends User {
    private String name;
    private String contacts;
    private String about;

    @ManyToMany
    @Cascade(org.hibernate.annotations.CascadeType.SAVE_UPDATE)
    @JoinTable(name = "subscribtions")
    private Set<UserCommon> subs;
}
