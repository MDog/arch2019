package edu.spbstu.arch.db.entity;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@Table(name = "users")
@DiscriminatorColumn(name="TypeUser", discriminatorType = DiscriminatorType.INTEGER)
public abstract class User {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @Column(name="TypeUser", insertable=false, updatable=false)
    private int TypeUser;

    private boolean authenticated;

    private String login;

    private String pwd;

    public boolean loginUser(String pwd) {
        String originpwd = this.getPwd();
        if (originpwd.equals(pwd)) {
            this.setAuthenticated(true);
            return true;
        }
        return false;
    }
}
