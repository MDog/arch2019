package edu.spbstu.arch.db.repository;

import edu.spbstu.arch.db.entity.UserAdvertiser;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public interface UserAdvertiserRepository extends UserRepository<UserAdvertiser> {
    @Query("select U from UserAdvertiser U where U.login = :login and U.TypeUser = 1")
    Optional<UserAdvertiser> getByLogin(@Param("login") String login);
}
