package edu.spbstu.arch.db.repository;

import edu.spbstu.arch.db.entity.Publication;
import edu.spbstu.arch.db.entity.UserCommon;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.List;

@Component
public interface PublicationRepository extends JpaRepository<Publication, Integer> {
    // call with PageRequest.of(0, 25)
    @Query("select p from Publication p where p.onlySubs = false or p.author in (:subs) or p.author = :author order by p.creationTime desc")
    //@Query("select p from Publication p where p.onlySubs = false or p.author in (select u.subs from UserCommon u where u = :user) order by p.creationTime desc")
    Page<Publication> getLastPublications(@Param("author") UserCommon author, @Param("subs") Collection<UserCommon> userSubs, Pageable pageable);
    //Page<Publication> getLastPublications(@Param("user") UserCommon user, Pageable pageable);

    @Query("select p from Publication p where p.onlySubs = false or p.author = :user order by p.creationTime desc")
    Page<Publication> getLastPublicationsNoSubs(@Param("user") UserCommon user, Pageable pageable);

    @Query("select p from Publication p where p.author = :author and (p.onlySubs = false or p.author = :viewer) order by p.creationTime desc")
    List<Publication> getAllByAuthorNoSubs(@Param("author") UserCommon author, @Param("viewer") UserCommon viewer);

    @Query("select p from Publication p where p.author = :author and (p.onlySubs = false or p.author = :viewer or p.author in (:subs)) order by p.creationTime desc")
    List<Publication> getAllByAuthor(@Param("author") UserCommon author, @Param("viewer") UserCommon viewer, @Param("subs") Collection<UserCommon> userSubs);
}
