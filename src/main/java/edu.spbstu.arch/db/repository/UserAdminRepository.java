package edu.spbstu.arch.db.repository;

import edu.spbstu.arch.db.entity.UserAdmin;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public interface UserAdminRepository extends UserRepository<UserAdmin> {
    @Query("select U from UserAdmin U where U.login = :login and U.TypeUser = 0")
    Optional<UserAdmin> getByLogin(@Param("login") String login);
}
