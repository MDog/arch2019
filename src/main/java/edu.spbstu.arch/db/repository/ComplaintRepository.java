package edu.spbstu.arch.db.repository;

import edu.spbstu.arch.db.entity.Complaint;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

@Component
public interface ComplaintRepository extends JpaRepository<Complaint, Integer> {
    //List<Complaint> findAllBy;
}
