package edu.spbstu.arch.db.repository;

import edu.spbstu.arch.db.entity.UserCommon;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public interface UserCommonRepository extends UserRepository<UserCommon> {
    @Query("select U from UserCommon U where U.login = :login and U.TypeUser = 2")
    Optional<UserCommon> getByLogin(@Param("login") String login);
}
