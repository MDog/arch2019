package edu.spbstu.arch.db.repository;

import edu.spbstu.arch.db.entity.Comment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

@Component
public interface CommentRepository extends JpaRepository<Comment, Integer> {
}
