package edu.spbstu.arch.controller;

import edu.spbstu.arch.db.entity.*;
import edu.spbstu.arch.model.ModelLogic;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class AuthenticationController {
    @Autowired
    ModelLogic service;

    @PostMapping("api/1.0/{userType}/{login}/auth")
    public User authenticate(@PathVariable String userType,
                             @PathVariable String login,
                             @RequestBody String passwd) {
        switch (userType.toLowerCase()) {
            case "common":
                return service.loginCommon(login, passwd);

            case "adv":
                return service.loginAdvertiser(login, passwd);

            case "admin":
                return service.loginAdmin(login, passwd);

            default:
                return null;
        }
    }

    @PostMapping(value="api/1.0/common")
    public boolean registerNewUserCommon(@RequestBody UserCommon user) {
        service.addNewUser(user);
        return true;
    }

    @PostMapping(value="api/1.0/admin")
    public boolean registerNewUserAdmin(@RequestBody UserAdmin user) {
        service.addNewAdministrator(user);
        return true;
    }

    @PostMapping(value="api/1.0/adv")
    public boolean registerNewUserAdv(@RequestBody UserAdvertiser user) {
        service.addNewAdvertiser(user);
        return true;
    }
}
