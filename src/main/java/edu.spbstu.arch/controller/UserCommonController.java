package edu.spbstu.arch.controller;

import edu.spbstu.arch.db.entity.Comment;
import edu.spbstu.arch.db.entity.Publication;
import edu.spbstu.arch.db.entity.UserCommon;
import edu.spbstu.arch.model.ModelLogic;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class UserCommonController {
    @Autowired
    ModelLogic model;

    /*@ExceptionHandler
    public String errorHandler(Exception ex) {
        System.err.print("Error: ");
        System.err.print(ex);
        return "@ExceptionHandler in " + getClass().getName();
    }*/

    @PostMapping("api/1.0/common/pub")
    @ResponseBody
    public Publication newPublication(@RequestBody Publication pub) {
        return model.newPublication(pub);
    }

    @GetMapping("api/1.0/common/pub")
    @ResponseBody
    public List<Publication> listPublications(@RequestParam("login") String userLogin,
                                              @RequestParam(name = "by", defaultValue = "", required = false) String author) {
        if (author != null && !author.isEmpty())
            return model.getPublicationsFrom(userLogin, author);
        else
            return model.getListPublicationsForUser(userLogin);
    }

    @GetMapping("api/1.0/common/pub/{pub_id}")
    @ResponseBody
    public Publication viewPublication(@PathVariable Integer pub_id,
                                       @RequestParam("login") String login) {
        return model.getPublicationForUser(pub_id, login);
    }

    @PostMapping("api/1.0/common/pub/{pub_id}/comment")
    public boolean newComment(@PathVariable Integer pub_id,
                              @RequestBody Comment comment) {
        return model.leaveACommentOnPublication(pub_id, comment);
    }

    @GetMapping("api/1.0/user/{id}")
    @ResponseBody
    public UserCommon getUserInfo(@PathVariable Integer id) {
        return model.getUserInfo(id);
    }

    @PostMapping("api/1.0/user/{author_id}/subscribe")
    public boolean subscribe(@PathVariable Integer author_id,
                             @RequestBody Integer subscriber) {
        return model.subscribeFor(author_id, subscriber, false);
    }

    @PostMapping("api/1.0/user/{author_id}/unsubscribe")
    public boolean unsubscribe(@PathVariable Integer author_id,
                               @RequestBody Integer subscriber) {
        return model.subscribeFor(author_id, subscriber, true);
    }
}
