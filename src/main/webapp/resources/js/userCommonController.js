function UserCommonCtrl($scope, $http, PublicationService, InfoShareService) {
    $scope.user = InfoShareService.getUser();

    $scope.editing = false;
    $scope.newPublication = {
        author: $scope.user,
        title: "",
        url: "",
        onlySubs: false
    };

    $scope.viewPublication = function(pubId) {
        window.location.href = "/#/pub/" + pubId;
    };

    $scope.publish = function () {
        var pub = new PublicationService();
        pub.author = $scope.newPublication.author;
        pub.title = $scope.newPublication.title;
        pub.url = $scope.newPublication.url;
        pub.onlySubs = $scope.newPublication.onlySubs;
        pub.$save({ login: $scope.user.login },
            function (responce) {
                $scope.editing = false;
                console.log("sent", $scope.newPublication);
                $scope.newPublication = { author: $scope.user };
                $scope.refreshPublications();
            },
            function (error) {
                alert(error.data.message);
            }
        );
    };

    $scope.refreshPublications = function () {
        PublicationService.query({ login: $scope.user.login }, function(value) {
            console.log("User pubs:", value);
            $scope.publications = value;
        });
    };

    $scope.refreshPublications();
}

app.controller('UserCommonCtrl', UserCommonCtrl);