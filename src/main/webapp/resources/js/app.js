var app = angular.module('app', ['ngRoute','ngResource']);
app.config(['$routeProvider', function($routeProvider) {
    $routeProvider
        .when('/', {
            redirectTo: 'login'
        })
        .when('/login', {
            templateUrl: 'view/loginPage.html',
            controller: 'LoginCtrl',
            controllerAs: 'loginCtrl'
        })
        .when('/common', {
            templateUrl: 'view/userCommonPage.html',
            controller: 'UserCommonCtrl',
            controllerAs: 'userCtrl'
        })
        .when('/common/view:uid', {
            templateUrl: 'view/viewUserPage.html',
            controller: 'ViewUserCtrl',
            controllerAs: 'viewUserCtrl'
        })
        .when('/admin', {
            templateUrl: 'view/userAdminPage.html',
            controller: 'UserAdminCtrl',
            controllerAs: 'userCtrl'
        })
        .when('/adv', {
            templateUrl: 'view/userAdvertiserPage.html',
            controller: 'UserAdvertiserCtrl',
            controllerAs: 'userCtrl'
        })
        .when('/pub/:pid', {
            templateUrl: 'view/publicationPage.html',
            controller: 'PublicationCtrl',
            controllerAs: 'pubCtrl'
        })

        .otherwise(
            { redirectTo: '/'}
        );
}]);
