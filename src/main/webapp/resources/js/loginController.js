function InfoShareService() {
    var user = {};
    return {
        setUser: function (value) {
            user = value;
        },
        getUser: function () {
            return user;
        }
    };
}
function UserService($resource) {
    return $resource('api/1.0/:userType', { userType: '@userType' });
}

function isEmpty(str) {
    return (!str || str.length === 0);
}

function LoginCtrl($scope, $http, UserService, InfoShareService) {
    $scope.isRegister = false;
    $scope.userType = 'common';

    $scope.isCommon = function () {
        return $scope.userType === 'common';
    };

    $scope.isAdvertiser = function () {
        return $scope.userType === 'adv';
    };

    $scope.signIn = function () {
        if (!$scope.login) {
            alert("Введите логин");
        } else if (!$scope.passwd) {
            alert("Введите пароль");
        } else {
            $http.post('api/1.0/' + $scope.userType + '/' + $scope.login + '/auth', $scope.passwd)
                .then(function (response) {
                    console.info("User:", response.data);
                    if (response.data == null || response.data.length === 0) {
                        alert("Некорректный логин или пароль");
                        $scope.login = "";
                        $scope.passwd = "";
                    } else {
                        InfoShareService.setUser(response.data);
                        window.location.href = '#/' + $scope.userType;
                    }
                }, function (error) {
                    alert("Некорректный логин или пароль");
                    console.error("Error:", error.data.message);
                    $scope.login = "";
                    $scope.passwd = "";
                });
        }
    };

    $scope.registerUser = function () {
        if (isEmpty($scope.loginReg)) {
            alert("Введите логин");
        } else if (isEmpty($scope.password1)) {
            alert("Введите пароль");
        } else if ($scope.password1 !== $scope.password2) {
            alert("Пароли не совпадают");
        } else {
            switch ($scope.userType) {
                case "adv": {
                    if (isEmpty($scope.name) || isEmpty($scope.contacts)) {
                        alert("Данные заполнены не полностью");
                        return;
                    }
                    break;
                }

                case "common": {
                    if (isEmpty($scope.name) || isEmpty($scope.about)) {
                        alert("Данные заполнены не полностью");
                        return;
                    }
                }
            }

            var user = new UserService();

            user.login = $scope.loginReg;
            user.pwd = $scope.password1;
            user.name = $scope.name;
            user.contacts = $scope.contacts;
            user.about = $scope.about;

            user.$save({userType: $scope.userType, login: $scope.loginReg},
                function (responce) {
                    $scope.login = $scope.loginReg;
                    $scope.passwd = $scope.password1;
                    $scope.signIn();

                    $scope.loginReg = "";
                    $scope.name = "";
                    $scope.password1 = "";
                    $scope.password2 = "";

                    $scope.contacts = "";
                    $scope.about = "";

                    $scope.isRegister = false;
                },
                function (error) {
                    alert(error.data.message);
                }
            );

            console.log("User:", user);
        }
    };

    $scope.setRegister = function () {
        $scope.isRegister = !$scope.isRegister;
    };
}

app
    .service('InfoShareService', InfoShareService)
    .factory('UserService', UserService)
    .controller('LoginCtrl', LoginCtrl);

