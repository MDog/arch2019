function UserInfoService($resource) {
    return $resource('api/1.0/user/:uid', { uid: '@uid' });
}

function ViewUserCtrl($scope, $http, $routeParams, PublicationService, UserInfoService, InfoShareService) {
    $scope.user = InfoShareService.getUser();
    $scope.author = {};

    $scope.refreshPublications = function () {
        PublicationService.query({ login: $scope.user.login, by: $scope.author.login }, function(value) {
            console.log("Publications:", value);
            $scope.publications = value;
        });
    };

    UserInfoService.get({ uid: $routeParams.uid },
        function (value) {
            $scope.author = value;
            $scope.refreshPublications();
            $scope.subscribed = $scope.isSubscribed();
        }
    );

    $scope.subscribe = function () {
        let author_id = $scope.author.id;
        let from = $scope.user.id;
        let sub = $scope.subscribed ? "/unsubscribe" : "/subscribe";

        $http.post("api/1.0/user/" + author_id + sub, from).then(
            function (response) {
                console.log("sub result:", response.data);
                if (response.data === true) {
                    UserInfoService.get({ uid: from }, function (value) {
                        $scope.user = value;
                        InfoShareService.setUser($scope.user);
                        $scope.subscribed = $scope.isSubscribed();
                        $scope.refreshPublications();
                    });
                } else {
                    alert("Fail!");
                }
            },
            function (error) {
                console.error(error);
                alert("Fail!");
            }
        );
    };

    $scope.isSubscribed = function () {
        let subSet = new Set($scope.user.subs.map(function (u) {
            return u.id;
        }));
        return subSet.has($scope.author.id);
    };

    $scope.subscribed = $scope.isSubscribed();

    $scope.viewPublication = function(pubId) {
        window.location.href = "/#/pub/" + pubId;
    };
}

app
    .service('UserInfoService', UserInfoService)
    .controller('ViewUserCtrl', ViewUserCtrl);