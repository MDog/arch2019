function PublicationService($resource) {
    return $resource('api/1.0/common/pub/:id', { id: '@id', login: '@login', by: '@by' });
}

function PublicationCtrl($scope, $http, $routeParams, PublicationService, InfoShareService) {
    $scope.user = InfoShareService.getUser();
    $scope.newComment = "";

    $scope.viewPublication = function(pubId) {
        window.location.href = "/#/pub/" + pubId;
    };

    $scope.updatePublication = function () {
        $scope.publication = PublicationService.get({ id: $routeParams.pid, login: $scope.user.login });
        console.log("Pub:", $scope.publication);
    };

    $scope.updatePublication();

    $scope.addComment = function () {
        if ($scope.newComment === "" || $scope.newComment.length === 0)
            return;

        $http.post("api/1.0/common/pub/" + $scope.publication.id + "/comment",
            { text: $scope.newComment, author: $scope.user, publication: $scope.publication })
            .then(function (response) {
                console.log(response);
                $scope.newComment = "";
                $scope.updatePublication();
            }, function (error) {
                console.error("Fail to comment:", error);
            });
    };
}

app
    .factory('PublicationService', PublicationService)
    .controller('PublicationCtrl', PublicationCtrl);