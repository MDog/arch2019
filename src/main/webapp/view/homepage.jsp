<!DOCTYPE html>
<html lang="ru" ng-app="app">
  <head>
    <meta charset="UTF-8">
    <link rel="shortcut icon" href="favicon.png" />
    <link rel='stylesheet' href='resources/css/style00.css' type='text/css' media='all'>
    <title>ДиванАрт [Arch2019]</title>
  </head>
  <body>
    <script src="resources/js/angular/angular.js"></script>
    <script src="resources/js/angular/angular-resource.js"></script>
    <script src="resources/js/angular/angular-route.js"></script>

    <script src="resources/js/app.js"></script>
    <script src="resources/js/loginController.js"></script>

    <script src="resources/js/publicationController.js"></script>

    <script src="resources/js/userCommonController.js"></script>
    <script src="resources/js/userAdminController.js"></script>
    <script src="resources/js/userAdvertiserController.js"></script>

    <script src="resources/js/viewUserController.js"></script>

    <div ng-view></div>
  </body>
</html>
