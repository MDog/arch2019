package edu.spbstu.arch;

import edu.spbstu.arch.model.ModelLogic;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@WebMvcTest(ModelLogic.class)
public class TestAuth {
    @Autowired
    ModelLogic facade;

    @Test
    public void TestAuthorization(){
        try {
            assert(facade.loginAdmin("op1", "111") != null);
            assert(facade.loginAdvertiser("us1", "111") != null);
            assert(facade.loginCommon("dr1", "111") != null);
        } catch (Exception e) {
            System.out.println("error");
        }
    }
}
